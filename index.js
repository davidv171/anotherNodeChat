//Init express
var express = require("express");
var app = express();
//TODO: Catch errors: port in use,...   
//tpl is the folder where the .jade files, which work as html substitutes are located
app.set('views', __dirname + '/tpl');
//init the jade view engine in express
app.set('view engine',"jade");
var http = require('http').Server(app);
var io = require('socket.io')(http);
//Choose a port, the number was chosen randomly
var port = 8090;
//Return "page" inside tpl when providing no arguments to the GET request
//add express to a file inside the /public folder
app.use(express.static(__dirname + '/public'));
//initialize a socket, that listens on the specified port(8080)
app.get("/",function(req,res){
    res.render("page");
    console.log("Rendered page");
});

console.log("Init done");
var listOfSocketIDs = [];
var listOfNicknames = [];
//any time there's a connection, execute an emit and any time someone clicks on send, emit the data
io.on('connection',function(socket){
    //socket.emit(message) triggers the function inside chat.js when send button is triggered and the logic for it is done
    console.log("Connection!");
    socket.emit('message',{message:'Connected to chat'});
    //An event that sends the list of connected ids and usernames into a textbox
    socket.emit('users',{listName:listOfNicknames,listID:listOfSocketIDs});
    console.log("Nicknames? " + listOfNicknames[0]);
    
    socket.on('send',function(data,name,socketID){
        console.log("Message sent" + data.socketID);
        if(!listOfSocketIDs.includes(data.socketID)){
            console.log("Comparing:" + listOfSocketIDs +" vs " + data.socketID);
            listOfSocketIDs.push(data.socketID);
            listOfNicknames.push(data.username); 
            io.sockets.emit('users',{listName:listOfNicknames,listID:listOfSocketIDs});   
        }
        console.log("DATA" + data);
        io.sockets.emit('message',data);
    });

});

http.listen(8090, function(){
    console.log('listening on *:8090');
  });
//TODO: When a new room is created set up a randomly generated link, that can then be used to join a certain session
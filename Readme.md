# Real time web chat #

A simple implementation of a real time chat experience, currently still under development

## Technologies used ##

- Node.js
- Express
- Jade
- Socket.io
- npm

## How it works ##

Simply open up the page in your browser, type into the chat box and you're ready to go.

## How to build ##

This program is yet not deployed to Heroku because of unknown issues. To build from source:

- Download the repository, either using the gitlab GUI(top right of the repository) or use the command git clone https://gitlab.com/davidv171/anotherNodeChat.git

- Run the server using node index.js

- Open the browser, type in 127.0.0.1:8090 in your browser address space

- Test it out

## How it looks like ##

Currently:

[When you write something](https://0x0.st/s_0j.png)

## Incoming features ##
- A list of connected users
- Login, a small database containing usernames and passwords(maybe)
- Coloring of different names(maybe)
- Members only chat

## Long term goals ##

- Dynamic link creation for on-the fly private chatrooms

- Password protected chatrooms


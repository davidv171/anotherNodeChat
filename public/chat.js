//LOGIC BEHIND CHAT
//send the message, written inside the textbox to everyone
//show it to everyone aswell
window.onload = function(){
    //Initialize variables
    //Array, that contains the input text
    console.log("Frontend ....");
    var messages= [];
    var users= [];
    var socket = io.connect('127.0.0.1:8090');
    //htmlTextField = the input text field, where the user inputs text, should go back to empty after pressing send
    //htmlSendButton = the input button
    //htmlChatContent = the content of the chat, what was written by every user
    var htmlTextField = document.getElementById("field");
    var htmlSendButton = document.getElementById("send");
    var htmlChatContent = document.getElementById("content");
    var htmlName = document.getElementById("name");
    var htmlPrivate = document.getElementById("private");
    var htmlUsers = document.getElementById("users");
    //Any time someone presses send(the listener for the send button is written in index.js):
    //triggered by: sock.sockets.emit('message',data);
    //TODO: Highlight messages
    socket.on('message',function(data){
        //Type out the message in the htmlTextField into the htmlChatContent if message was received
        //Keep the other chats
        if(data.message){
            //From w3c:The push() method adds new items to the end of an array, and returns the new length.
            //Example: ["hi","hello"] ---push("wazzzaaap")--->["hi","hello","wazzzaaap"]
            messages.push(data);
            //put messages into the HTML code
            var html = '';
            console.log("Currently in messages" + data.message);
            for(var i = 0;i<messages.length ;i++){
                //Put it into a string and add a HTML defined symbol sequence for a line break
                //Add username in front of it in bold
                if(messages[i].username==null){
                    html+=messages[i].message + '<br />';
                }
                else{
                    html+='<b>'+ messages[i].username + ': </b>' + messages[i].message   + '<br />';
                }

            }
            //Add the message formatted into HTML into the chat content box
            htmlChatContent.innerHTML = html;
            //When sending clear the input field also

            htmlTextField.value = "";
        }
        else{
            //This means there was an error
            //Put error text inside the users text box
            console.log("Error");
            htmlTextField.innerHTML = "There was an sending error!";
        }   
    });
    //Any time there's a new user, and when you first connect run this
    //Print out the connected users and their unique socket.io ids
    socket.on('users',function(data){
        var usersHtml='';
        console.log("New user!"+ data.listID[0]);
        for(var i = 0;i<data.listID.length;i++){
            
            usersHtml+= '<b>' + data.listName[i] + '</b>(' + data.listID[i] + ')</br>';
        }
        htmlUsers.innerHTML= usersHtml;
        
    });
    //Listen for the HTML click on the send button,currently not functional
    //Check for empty username and don't allow anything to be sent while username is empty
    htmlSendButton.onclick =function(){
        console.log("Send button pressed");
        var text =  htmlTextField.value;
        var name = htmlName.value;
        if(name==''){
            alert("Name must not be empty");
        }
        else{
            socket.emit('send', { message: text,username:name,socketID:socket.id});
            console.log("Sent by:" + socket.id);
            htmlName.remove();

        }

        
    };
    //TODO: Redirect to a freshly created random link
    htmlPrivate.onclick = function(){
        console.log("Generate private room");
        
    }


}
